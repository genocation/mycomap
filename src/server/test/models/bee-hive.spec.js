import RAM from 'random-access-memory'
import Corestore from 'corestore'
import Networker from '@corestore/networker'
import crypto from 'crypto'

import BeeHive from '../../models/bee-hive.js'
import HyperClient from '../../lib/hyper-client.js'
import DHTStub from '../lib/dht.stub.js'
import { sleep } from '../spec-helper.js'
import { hex, shortHex } from '../../utils.js'

describe('creating a new hive', function () {
  let client

  beforeEach(async () => {
    client = new HyperClient()
    await client.ready()
  })

  afterEach(async () => {
    await client.close()
  })

  it('is successful', async function () {
    const hive = new BeeHive(client)
    expect(hive.pid).toBeNull()
    await hive.writer()
    await hive.open()
    expect(hive.db).toBeDefined()
    expect(hive.pid).toBe(shortHex(hive.db.feed.key))
    expect(hive.sid).toBe(hive.pid)
    expect(hive.bees).toBeInstanceOf(Map)
    expect(hive.bees.size).toBe(1)

    await hive.close()
  })
})

describe('joining a new hive', function () {
  let client

  beforeEach(async () => {
    client = new HyperClient()
    await client.ready()
  })

  afterEach(async () => {
    await client.close()
  })

  it('is successful using an invite code', async function () {
    const address = hex(crypto.randomBytes(32))
    const pid = shortHex(address)
    const hive = new BeeHive(client, pid)
    expect(hive.pid).toBe(pid)

    await hive.writer()
    await hive.seed(address)
    await hive.open()

    expect(hive.db).toBeDefined()
    expect(hive.sid).toBe(shortHex(hive.db.feed.key))

    expect(hive.bees.size).toBe(2)
    expect(hive.bees.has(address)).toBe(true)

    await hive.close()
  })
})

describe('loading a hive from storage', function () {
  let client

  beforeEach(async () => {
    client = new HyperClient()
    await client.ready()
  })

  afterEach(async () => {
    await client.close()
  })

  it('is successful', async function () {
    const addrs = []

    for (const i of [0, 1, 2, 3, 4, 5]) {
      const reader = client.corestore.get({ key: crypto.randomBytes(32) })
      await reader.ready()
      addrs.push(hex(reader.key))
      await reader.close()
    }

    const writer = client.corestore.get()
    await writer.ready()
    const address = hex(writer.key)
    addrs.push(address)
    await writer.close()

    const pid = shortHex(writer.key)
    const hive = new BeeHive(client, pid, addrs)
    expect(hive.pid).toBe(pid)

    await hive.writer(address)
    hive.seed(addrs)
    await hive.open()

    expect(hive.sid).toBe(pid)
    expect(hive.bees.size).toBe(addrs.length)

    await hive.close()
  })
})

describe('replication', function () {
  let dht
  let client1, client2, client3

  beforeEach(async () => {
    dht = new DHTStub()
    await dht.ready()
    client1 = dht.createClient()
    client2 = dht.createClient()
    client3 = dht.createClient()
    await Promise.all([
      client1.ready(),
      client2.ready(),
      client3.ready()
    ])
  })

  afterEach(async () => {
    await Promise.all([
      client1.close(),
      client2.close(),
      client3.close()
    ])
    await dht.close()
  })

  it('adds new remote bees', async function () {
    const hive1 = new BeeHive(client1)
    await hive1.writer()
    await hive1.open()

    const hive2 = new BeeHive(client2, hive1.pid)
    await hive2.writer()
    hive2.seed(hive1.address)
    await hive2.open()

    await sleep(500)

    expect(hive1.bees.has(hex(hive2.db.feed.key))).toBe(true)
    expect(hive2.bees.has(hex(hive1.db.feed.key))).toBe(true)

    expect(hive1.peers.length).toBe(1)
    expect(hive2.peers.length).toBe(1)

    const hive3 = new BeeHive(client3, hive1.pid)
    await hive3.writer()
    hive3.seed(hive2.address)
    await hive3.open()

    await sleep(500)

    expect(hive1.bees.has(hex(hive3.db.feed.key))).toBe(true)
    expect(hive2.bees.has(hex(hive3.db.feed.key))).toBe(true)

    expect(hive1.peers.length).toBe(2)
    expect(hive2.peers.length).toBe(2)

    await Promise.all([
      hive1.close(),
      hive2.close(),
      hive3.close()
    ])
  })
})
