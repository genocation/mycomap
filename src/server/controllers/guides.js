import '../types.js'
import debug from 'debug'
import Hyperbee from 'hyperbee'

import Guide from '../models/guide.js'
import HyperController from './hyper.js'

import {
  hex, shortHex, generateID,
  latToLex, lngToLex, pick
} from '../utils.js'

const log = debug('mycomap:api:guides')
const { assign, values } = Object
const streamOptions = { gt: 'guide/', lt: 'guide0' }

export default class GuidesController extends HyperController {
  /**
   * @param {HyperClient} client - an object containing corestore and networker
   * @param {Hyperbee} localdb - hyperbee instance wrapping a primary hypercore
   */
  constructor (client, localdb, indexdb) {
    super(client, localdb)
    this.indexdb = indexdb
    this.guides = new Map()

    this.create = this.create.bind(this)
    this.inspect = this.inspect.bind(this)
    this.list = this.list.bind(this)
    this.join = this.join.bind(this)
    this.invite = this.invite.bind(this)
    this.addSpot = this.addSpot.bind(this)
    this.listGuideSpots = this.listGuideSpots.bind(this)
  }

  /**
   * bootstrap on startup, load all guides into memory
   * then start replicating on all of them
   */
  async start () {
    await super.start()
    await this._cacheAllGuides()

    log({
      msg: `${this.constructor.name} ready`,
      payload: {
        guides: Array.from(this.guides.keys())
      }
    })
  }

  async stop () {
    const proms = []
    for (const guide of this.guides.values()) {
      proms.push(guide.close())
    }
    await Promise.all(proms)
  }

  /**
   * GET /api/guides
   * @param {Object} req - an express request object
   * @param {Object} res - an express response object
   */
  async list (req, res) {
    try {
      const proms = []
      for (const [pid, guide] of this.guides) {
        proms.push(guide.metadata())
      }
      const guides = await Promise.all(proms)
      return res.json(guides)
    } catch (err) {
      log(err)
      return res.sendStatus(500)
    }
  }

  /**
   * POST /api/guides
   * @param {Object} req - an express request object
   * @param {Object} res - an express response object
   */
  async create (req, res) {
    try {
      // create a brand new guide
      const guide = new Guide(this.client)
      // index new remote dbees when added
      guide.on('add-bee', this._indexHyperbee(guide))
      // index new spots when added
      guide.on('new-entry', this._indexSpot(guide))
      // index guide writer in localdb when its open
      guide.on('open', () => {
        const writerKey = `guide/${guide.pid}/writer/${guide.sid}`
        log({ msg: '[INDEX-BEE] adding guide reference', payload: { writerKey } })
        this.localdb.put(writerKey, guide.address)
      })
      // create a writer
      await guide.writer()
      // then open the guide
      await guide.open()
      // cache the guide
      this.guides.set(guide.pid, guide)
      // save a metadata record using request body parameters
      const metadata = {
        pid: guide.pid,
        ...guideParams(req.body),
        createdAt: Date.now(),
        createdBy: {}
      }
      await guide.db.put(`metadata/${metadata.createdAt}`, metadata)
      log({ msg: `[CREATE-GUIDE] ${guide.pid}@${guide.address}`, payload: metadata })
      return res.json(metadata)
    } catch (err) {
      log(err)
      return res.sendStatus(500)
    }
  }

  /**
   * GET /api/guides/:pid/invite
   * @param {Object} req - an express request object
   * @param {Object} res - an express response object
   */
  async invite (req, res) {
    try {
      // fetch the relevant guide
      const guide = this.guides.get(req.params.pid)
      if (!guide) throw new Error(`not found: guides/${guide.pid} does not exist`)
      // otherwise, return an invite code
      const invite = { code: guide.code }
      log({ msg: '[INVITE-GUIDE] invite generated', payload: invite })
      return res.json(invite)
    } catch (err) {
      log({ msg: '[INVITE-GUIDE] failed to generate invite', err })
      return res.sendStatus(500)
    }
  }

  /**
   * POST /api/guides/:pid/join
   * @param {Object} req - an express request object
   * @param {Object} res - an express response object
   */
  async join (req, res) {
    try {
      const { code } = req.body
      const [ pid, remoteAddress ] = code.split('@')
      // return if the guide already exists in the cache
      if (this.guides.has(pid)) {
        throw new Error(`found: guides/${pid} already exists`)
      }
      // create a guide, using the provided remoteAddress
      const guide = new Guide(this.client, pid)
      // index new remote dbees when added
      guide.on('add-bee', this._indexHyperbee(guide))
      // index new spots when added
      guide.on('new-entry', this._indexSpot(guide))
      // index local bees when guide is open
      guide.on('open', async () => {
        const batch = this.localdb.batch()
        const writerKey = `guide/${guide.pid}/writer/${guide.sid}`
        const readerKey = `guide/${guide.pid}/reader/${shortHex(remoteAddress)}`
        await batch.put(readerKey, remoteAddress)
        await batch.put(writerKey, guide.address)
        log({ msg: '[INDEX-BEE] adding guide references', payload: { writerKey, readerKey }})
        batch.flush()
      })
      // generate the writer
      await guide.writer()
      // seed the address
      guide.seed(remoteAddress)
      // make all guide feeds ready
      await guide.open()
      // cache locally
      this.guides.set(guide.pid, guide)
      // save a metadata record for the guide
      const metadata = {
        pid: guide.pid,
        createdAt: Date.now(),
        createdBy: {}
      }
      await guide.db.put(`metadata/${metadata.createdAt}`, metadata)
      log({ msg: `[JOIN-GUIDE] ${guide.pid}@${guide.address}\n`, payload: metadata })
      return res.json(metadata)
    } catch (err) {
      log(err)
      return res
        .status(400)
        .json({ errors: [err.message] })
    }
  }

  async listGuideSpots (req, res) {
    try {
      const guide = this.guides.get(req.params.pid)
      // TODO: assert guide exists
      const spots = []
      // in leveldb, get everything from keys:
      // guide/PID/lat/
      // guide/PID/lat0
      const stream = this.indexdb.createReadStream({
        gt: `guide/${guide.pid}/lat/`,
        lt: `guide/${guide.pid}/lat0`
      })

      for await (const entry of stream) {
        const [_, sid, spotId, address, seq] = entry.value.split('@')
        const bee = guide.bees.get(address)
        const spot = await bee.get(`spot/${spotId}`)
        spots.push(spot.value)
      }

      return res.json(spots)
    } catch (err) {
      log(err)
      return res.sendStatus(500)
    }
  }

  /**
   * POST /api/guides/:pid/spots
   * @param {Object} req - an express request object
   * @param {Object} res - an express response object
   */
  async addSpot (req, res) {
    try {
      // get the guide
      const guide = this.guides.get(req.params.pid)
      // TODO: assert guide exists
      // add spot to the writable guide db
      const entry = await guide.addSpot(spotParams(req.body))
      const spot = entry.value
      // add index to level indexdb
      // TODO: tag key
      const reference = `${guide.pid}@${guide.sid}@${spot.id}@${guide.address}@${entry.seq}`
      const latKey = `guide/${guide.pid}/lat/${latToLex(spot.lat)}`
      const lngKey = `guide/${guide.pid}/lng/${lngToLex(spot.lng)}`
      // store indexes
      await this.indexdb.batch()
        .put(latKey, reference)
        .put(lngKey, reference)
        .write()
      // return the spot data
      return res.json(spot)
    } catch (err) {
      log(err)
      return res.sendStatus(500)
    }
  }

  /**
   * GET /api/inspect
   * @param {Object} req - an express request object
   * @param {Object} res - an express response object
   */
  async inspect (req, res) {
    log(`\n\ninspecting localdb\n`)
    for await (const entry of this.localdb.createReadStream()) {
      log(entry)
    }
    for (const [pid, guide] of this.guides) {
      log(`\n\ninspecting ${pid}\n`)
      for await (const entry of guide.createReadStream()) {
        log(entry)
      }
    }
    log(`\n\ninspecting indexdb\n`)
    for await (const entry of this.indexdb.createReadStream()) {
      log(entry)
    }
    return res.sendStatus(200)
  }

  /**
   * guide entries are saved in localdb in the following format:
   * { key: 'guides/$PID/$ROLE/$SID', value: $address }
   * where $ROLE is either 'reader' or 'writer'
   * here we load all guide references
   * then construct and cache the guide
   */
  async _cacheAllGuides () {
    const byPid = {
      // [pid]: {
      //   writer: 'address',
      //   readers: {
      //     [sid]: 'address',
      //     ...
      //   }
      // }
    }
    const stream = this.localdb.createReadStream(streamOptions)
    // get all guide pids, corresponding lists of addresses and sort by role
    for await (const entry of stream) {
      const { key, value: address } = entry
      const [ _, pid, role, sid ] = key.split('/')
      const guideOpts = byPid[pid] || { writer: null, readers: {} }
      if (sid && role === 'reader') {
        guideOpts.readers[sid] = address
      } else {
        guideOpts.writer = address
      }
      byPid[pid] = guideOpts
    }
    // create the writer, seed reader addresses, open guide and then cache
    const proms = []
    for (const pid in byPid) {
      const { writer, readers } = byPid[pid]
      const guide = new Guide(this.client, pid)
      // setup listeners to index new guide cores
      guide.on('add-bee', this._indexHyperbee(guide))
      // index new spots when added
      guide.on('new-entry', this._indexSpot(guide))
      // set the writer
      guide.writer(writer)
      // and the readers
      guide.seed(Object.values(readers))
      // open the guide
      proms.push(guide.open())
      // cache the guide
      this.guides.set(pid, guide)
    }

    await Promise.all(proms)
  }

  _indexHyperbee (guide) {
    const self = this
    return function onRemoteBee (dbee) {
      log({ msg: '[REMOTE-BEE] indexing new hyperbee', payload: hex(dbee.feed.key) })
      self.localdb.put(`guide/${guide.pid}/reader/${shortHex(dbee.feed.key)}`, hex(dbee.feed.key))
    }
  }

  _indexSpot (guide) {
    const self = this
    return function onNewEntry (entry) {
      // we check if it's a spot
      const type = entry.key.split('/')[0]
      if (type !== 'spot') return
      // if it's a spot we index it for this guide in leveldb
      const spot = entry.value
      const reference = `${guide.pid}@${guide.sid}@${spot.id}@${entry.address}@${entry.seq}`
      const latKey = `guide/${guide.pid}/lat/${latToLex(spot.lat)}`
      const lngKey = `guide/${guide.pid}/lng/${lngToLex(spot.lng)}`
      // store indices
      log({ msg: '[NEW-SPOT] indexing new spot', payload: spot })
      self.indexdb.batch()
        .put(latKey, reference)
        .put(lngKey, reference)
        .write()
    }
  }
}

function guideParams (params) {
  return pick(params, ['name', 'description'])
}

function spotParams (params) {
  return pick(params, ['name', 'description', 'lat', 'lng', 'tags'])
}
