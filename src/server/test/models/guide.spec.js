import { hex, shortHex } from '../../utils.js'
import { sleep } from '../spec-helper.js'
import DHTStub from '../lib/dht.stub.js'
import HyperClient from '../../lib/hyper-client.js'

import Guide from '../../models/guide.js'

describe('guide', function () {
  let dht
  let client1, client2

  beforeEach(async () => {
    dht = new DHTStub()
    await dht.ready()
    client1 = dht.createClient()
    client2 = dht.createClient()
    await Promise.all([
      client1.ready(),
      client2.ready()
    ])
  })

  afterEach(async () => {
    await Promise.all([
      client1.close(),
      client2.close()
    ])
    await dht.close()
  })

  it('compiles metadata', async function () {
    const guide1 = new Guide(client1)
    await guide1.writer()
    await guide1.open()

    const metadata1 = {
      pid: guide1.pid,
      name: 'guideland',
      description: 'this is my first guide',
      createdAt: Date.now()
    }

    await guide1.db.put(`metadata/${Date.now()}`, metadata1)

    await sleep(1000)

    const result1 = await guide1.metadata()

    expect(result1.pid).toBe(guide1.pid)
    expect(result1.name).toBe(metadata1.name)
    expect(result1.description).toBe(metadata1.description)

    const guide2 = new Guide(client2, guide1.pid)
    await guide2.writer()
    await guide2.seed(guide1.address)
    await guide2.open()

    const metadata2 = {
      pid: guide1.pid,
      createdAt: Date.now()
    }

    await guide2.db.put(`metadata/${Date.now()}`, metadata2)

    await sleep(1000)

    const result2 = await guide1.metadata()

    expect(result2.pid).toBe(guide1.pid)
    expect(result2.name).toBe(metadata1.name)
    expect(result2.description).toBe(metadata1.description)
    expect(result2.createdAt).toBe(metadata1.createdAt)
    expect(result2.updatedAt).toBe(metadata2.createdAt)

    await Promise.all([
      guide1.close(),
      guide2.close()
    ])
  })
})
