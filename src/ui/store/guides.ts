import axios from 'axios'
import { Module, GetterTree, MutationTree, ActionTree } from 'vuex'

import { RootState, GuidesState, GuideModel, SpotModel } from './types'

const state: GuidesState = {
  guides: [],
  spots: []
}

const mutations: MutationTree<GuidesState> = {
  setGuides(state, guides: Array<GuideModel>) {
    state.guides = guides
  },

  setGuide(state, guide: GuideModel) {
    state.guides.push(guide)
  },

  setSpots(state, spots: Array<SpotModel>) {
    state.spots = spots
  },

  setSpot(state, spot: SpotModel) {
    state.spots.push(spot)
  }
}

const actions: ActionTree<GuidesState, RootState> = {
  fetchAllGuides({ commit }): void {
    axios({
      url: '/api/guides'
    }).then((response) => {
      commit('setGuides', response.data)
    }, (error) => {
      console.log(error)
    })
  },

  async fetchAllSpots({ commit }): Promise<any> {
   try {
      // const response = await axios.get(`/api/guides/${pid}/spots`)
      const response = { data: [] }
      commit('setSpots', response.data)
      return response.data
    } catch (err) {
      console.error(err)
    }
  },

  async fetchGuideSpots({ commit }, pid): Promise<any> {
    try {
      const response = await axios.get(`/api/guides/${pid}/spots`)
      commit('setSpots', response.data)
      return response.data
    } catch (err) {
      console.error(err)
    }
  },

  async createGuide({ commit }, params): Promise<any> {
    try {
      const response = await axios.post('/api/guides', params)
      commit('setGuide', response.data)
      return response.data
    } catch (err) {
      console.error(err)
    }
  },

  createSpot({ commit }, { pid, payload }): Promise<any> {
    return new Promise((resolve, reject) => {
      axios.post(`/api/guides/${pid}/spots`, payload).then((response) => {
        commit('setSpot', response.data)
        resolve(true)
      }).catch((err) => {
        reject(err)
      })
    })
  }
}

const getters: GetterTree<GuidesState, RootState> = {
  getGuides(state) : Array<GuideModel> {
    return state.guides
  },

  getSpots(state) : Array<SpotModel> {
    return state.spots
  }
}

export default {
  namespaced: true as true,
  state,
  mutations,
  actions,
  getters
}
