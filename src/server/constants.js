const APP_NAME = 'mycomap'

export const DEFAULT_STORAGE = './storage'
export const DEFAULT_PORT = 5000
export const CORESTORE_NAMESPACE = APP_NAME
export const HYPERHOST = APP_NAME

export const defaultEncoding = {
  keyEncoding: 'utf-8',
  valueEncoding: 'json'
}
