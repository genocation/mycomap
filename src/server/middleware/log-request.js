import debug from 'debug'
import { pick } from '../utils.js'
const log = debug('mycomap:request')

export default function logRequest (req, res, next) {
  log(pick(req, ['url', 'method', 'params', 'query', 'body']))
  next()
}
