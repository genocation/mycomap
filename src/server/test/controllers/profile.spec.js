import { mockRequest, mockResponse, mockReadStream } from '../spec-helper.js'
import ProfileController from '../../controllers/profile.js'
const { assign } = Object

describe('ProfileController#get', function () {
  it('fetches your profile from your localdb', async function () {
    const msg = {
      seq: 1,
      key: 'profile/1623345254633',
      value: { nickname: 'pretty fly agaric' }
    }

    const req = mockRequest()
    const res = mockResponse()
    const localdb = { createReadStream: mockReadStream([msg]) }
    const profileController = new ProfileController({}, localdb)

    await profileController.get(req, res)

    expect(localdb.createReadStream).toHaveBeenCalledWith({ gt: 'profile/', lt: 'profile0' })
    expect(res.json).toHaveBeenCalledWith({
      ...msg.value,
      color: "hsl(49, 99%, 77%)",
      updatedAt: 1623345254633
    })
  })

  it('builds profile state from multiple entries', async function () {
    const updates = [{
      seq: 1,
      key: 'profile/1623345254633',
      value: { nickname: 'pretty fly agaric' }
    }, {
      seq: 2,
      key: 'profile/1623345254634',
      value: { avatar: 'hyper://afda546ada1d277bf6c5fe3eb9c7773498b94ad0d47bed56869903844029b33b' }
    }, {
      seq: 3,
      key: 'profile/1623345254635',
      value: { nickname: 'old man of the woods' }
    }]

    const req = mockRequest()
    const res = mockResponse()
    const localdb = { createReadStream: mockReadStream(updates) }
    const profileController = new ProfileController({}, localdb)

    await profileController.get(req, res)

    expect(localdb.createReadStream).toHaveBeenCalledWith({ gt: 'profile/', lt: 'profile0' })
    let currentProfile = assign({}, updates[0].value, updates[1].value, updates[2].value, {
      updatedAt: 1623345254635,
      color: "hsl(76, 88%, 30%)"
    })
    expect(res.json).toHaveBeenCalledWith(currentProfile)
  })
})

describe('ProfileController#put', function () {
  it('saves your profile in your localdb', async function () {
    const realDateNow = Date.now.bind(global.Date)
    const timestamp = 1623345254633
    global.Date.now = jest.fn(() => timestamp)

    const requestBody = { nickname: 'pretty fly agaric' }

    const req = mockRequest(requestBody)
    const res = mockResponse()

    const localdb = { put: jest.fn() }

    const profileController = new ProfileController({}, localdb)
    await profileController.put(req, res)

    expect(localdb.put).toHaveBeenCalledWith(`profile/${timestamp}`, requestBody)
    expect(res.sendStatus).toHaveBeenCalledWith(200)

    global.Date.now = realDateNow
  })
})
