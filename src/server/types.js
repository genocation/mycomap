/**
 * ATTENTION:
 * types.js defines jsdoc typedefs for including and using project wide
 * when creating a new class, please add a typedef and its properties here
 * jsdoc typing is NOT enforced, this is merely a documentation standard
 *
 * add raw hyper* types
 * @typedef {Object} Corestore
 * @typedef {Object} CorestoreNetworker
 * @typedef {Object} Hyperbee
 *
 * hyperclient acts as a wrapper for these
 * @typedef {Object} HyperClient
 * @property {Corestore} corestore
 * @property {CorestoreNetworker} networker
 * @property {string|function} storage
 *
 * we make use of a DHT stub in our tests
 * @typedef {Object} DHTStub
 * @property {Object} dht - a @hyperswarm/dht instance
 * @property {Object} bootstrap - the bootstrap url
 *
 * we store a nickname in our localdb as our 'profile'
 * @typedef {Object} Profile
 * @property {string} nickname - your nickname
 * @property {string} colour - the hash colour of your nickname
 * @property {number} updatedAt - when the last profile entry was published
 *
 * beehive is our primary class for grouping hyperbees
 * @typedef {Object} BeeHive
 * @property {string} pid - the primary id of the bee-hive
 * @property {string} sid - the secondary id (short hex of the writable feed key)
 * @property {Hyperbee} db - the writable hyperbee
 * @property {Map} bees - all cached hyperbees
 * @property {string} address - the writable feed key
 * @property {string} code - an invite code for new peers to join
 * @property {Array.<string>} peers - an array of addresses of other hyperbees
 * @property {boolean} isOpen - check if a beehive is open
 *
 * guide class extends from beehive
 * @typedef {BeeHive} Guide
 *
 * guide publishes metadata entries
 * @typedef {Object} Metadata
 * @property {string} pid - the pid of the beehive / guide
 * @property {number} createdAt - when the first metadata entry was published
 * @property {Object} createdBy - the guide creator's details
 * @property {number} updatedAt - when the last metadata entry was published
 * @property {string} name - the guide name
 * @property {string} description - the guide description
 *
 * we also publish spot entries
 * @typedef {Object} Spot
 * @property {string} name
 * @property {string} description
 * @property {number} lat
 * @property {number} lng
 * @property {Array.<string>} tags
 */
