import request from 'supertest'
import Server from '../../server.js'
import { tmpdir, cleanup } from '../spec-helper'

describe('GET /api/profile', () => {
  let server
  let storage

  beforeEach(async () => {
    storage = await tmpdir()
    server = new Server({ storage, port: 9111 })
    await server.setup()
    await server.localdb.put(`profile/${Date.now()}`, { nickname: 'lactarius deliciosus' })
  })

  afterEach(async () => {
    await server.stop()
    await cleanup(storage)
  })

  it('should return a profile object', async () => {
    const res = await request(server.app)
      .get('/api/profile')

    expect(res.statusCode).toEqual(200)
    expect(res.body).toHaveProperty('nickname')
  })
})

describe('POST /api/profile', () => {
  let server
  let storage

  beforeEach(async () => {
    storage = await tmpdir()
    server = new Server({ storage, port: 9111 })
    await server.setup()
  })

  afterEach(async () => {
    await server.stop()
    await cleanup(storage)
  })

  it('should return a 200 with valid parameters', async () => {
    const res = await request(server.app)
      .post('/api/profile')
      .send({ nickname: 'lactarius deliciosus' })

    expect(res.statusCode).toEqual(200)
  })

  it('should return a 400 with invalid parameters', async () => {
    const res = await request(server.app)
      .post('/api/profile')
      .send({ nickname: 12345 })

    expect(res.statusCode).toEqual(400)
  })
})
