import '../../types.js'
import http from 'http'
import dht from '@hyperswarm/dht'
import HyperClient from '../../lib/hyper-client.js'

/**
 * stub out non-local network activity on the DHT by removing
 * bootstrap nodes, peer discovery only occurs over localhost
 */
export default class DHTStub {
  #dht = null
  #port = null

  constructor () {
    this.#dht = dht({
      bootstrap: false
    })
  }

  /**
   * fetch a random free port
   * then tell the dht-rpc to
   * begin listening on that port
   */
  async ready () {
    this.#port = await this.#getPort()
    this.dht.listen(this.#port)
    return new Promise((resolve) => (
      this.dht.once('listening', resolve)
    ))
  }

  /**
   * create a new client to use in tests
   * @returns {HyperClient}
   */
  createClient () {
    return new HyperClient({
      bootstrap: this.bootstrap
    })
  }

  /**
   * close the DHT
   */
  async close () {
    this.dht.destroy()
    this.#dht = null
  }

  /**
   * fetch the DHT instance
   */
  get dht () {
    return this.#dht
  }

  /**
   * bootstrap url for the client to use
   * to force peer discovery over localhost only
   */
  get bootstrap () {
    return {
      host: 'localhost',
      port: this.#port
    }
  }

  /**
   * fire up and close a new web server until
   * we discovery an open and free port
   */
  async #getPort () {
    // spin up a new http server
    const server = http.createServer()
    // listen on a random port
    await server.listen(0)
    return new Promise((resolve) => {
      // successfully opened port, we can use this one
      server.on('listening', () => {
        const port = server.address().port
        server.once('close', () => resolve(port))
        server.close()
      })
      // failed to open port, its in use, try again
      server.on('error', () => {
        server.once('close', () => this.#getPort().then(resolve))
        server.close()
      })
    })
  }
}
