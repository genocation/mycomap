import { Router } from 'express'
import debug from 'debug'
import swaggerUi from 'swagger-ui-express'
import swaggerJsDoc from 'swagger-jsdoc'
import fs from 'fs'
import requestValidationMiddleware from './middleware/request-validation.js'

const log = debug('mycomap:api')
const settings = JSON.parse(fs.readFileSync('./package.json'))
const swaggerSpec = swaggerJsDoc({
  definition: {
    openapi: '3.0.0',
    info: {
      title: settings.name,
      version: settings.version
    }
  },
  apis: ['./routes.js']
})

export default function routes (controllers, validators) {
  const router = Router()

  router.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerSpec))

  /**
   * @openapi
   * /api/profile:
   *   get:
   *     description: get profile data
   *     responses:
   *       200:
   *         description: returns profile data
   *         content:
   *           application/json:
   *             schema:
   *               type: object
   *               properties:
   *                 nickname:
   *                   type: string
   */
  router.get('/api/profile', controllers.profile.get)

  /**
   * @openapi
   * /api/profile:
   *   post:
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               nickname:
   *                 type: string
   *                 example: pretty fly agaric
   *     responses:
   *       200:
   *         description: stores profile data
   */
  router.post('/api/profile', validators.profile.put, requestValidationMiddleware, controllers.profile.put)

 /**
   * @openapi
   * /api/guides:
   *   get:
   *     description: get list of guides
   *     responses:
   *       200:
   *         description: returns all guides
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               properties:
   *                 nickname:
   *                   type: string
   */
  router.get('/api/guides', controllers.guides.list)

  /**
   * @openapi
   * /api/guides:
   *   post:
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               name:
   *                 type: string
   *                 example: my awesome guide
   *               description:
   *                 type: string
   *                 example: this is where I store my secret mushies
   *     responses:
   *       200:
   *         description: creates a guide, stores its metadata and starts seeding
   */
  router.post('/api/guides', controllers.guides.create)

  /**
   * @openapi
   * /api/guides/join:
   *   post:
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               code:
   *                 type: string
   *                 example: 0a5973f1@0a5973f1a9925fc534a1b199b91af5f6de670ca91392e96a3ca0b31754364410
   *     responses:
   *       200:
   *         description: joins a guide
   */
  router.post('/api/guides/join', controllers.guides.join)

 /**
   * @openapi
   * /api/guides/{pid}/invite:
   *   get:
   *     parameters:
   *       - in: path
   *         name: pid
   *         schema:
   *           type: string
   *         required: true
   *         description: primary guide id
   *     description: get an invite to a guide
   *     responses:
   *       200:
   *         description: returns the guide's invite code
   *         content:
   *           application/json:
   *             schema:
   *               type: string
   *               properties:
   *                 code:
   *                   type: string
   */
  router.get('/api/guides/:pid/invite', controllers.guides.invite)

  /**
   * @openapi
   * /api/guides/{pid}/spots:
   *   get:
   *     parameters:
   *       - in: path
   *         name: pid
   *         schema:
   *           type: string
   *         required: true
   *         description: primary guide id
   *     description: get list of spots for a guide
   *     responses:
   *       200:
   *         description: returns all spots for a guide
   *         content:
   *           application/json:
   *             schema:
   *               type: array
   *               properties:
   *                 nickname:
   *                   type: string
   */
  router.get('/api/guides/:pid/spots', controllers.guides.listGuideSpots)

  /**
   * @openapi
   * /api/guides/{pid}/spots:
   *   post:
   *     parameters:
   *       - in: path
   *         name: pid
   *         schema:
   *           type: string
   *         required: true
   *         description: primary guide id
   *     requestBody:
   *       required: true
   *       content:
   *         application/json:
   *           schema:
   *             type: object
   *             properties:
   *               lat:
   *                 type: float
   *                 example: 42.9453986
   *               lng:
   *                 type: float
   *                 example: -2.7196453
   *               name:
   *                 type: string
   *                 example: boletus edulis
   *               description:
   *                 type: string
   *                 example: so so deliciously tasty, ooohh so tasy
   *               tags:
   *                 type: array
   *                 items:
   *                   type: string
   *                   example: cep
   *     responses:
   *       200:
   *         description: creates and saves a spot for this guide
   */
  router.post('/api/guides/:pid/spots', validators.guides.addSpot, controllers.guides.addSpot)

  /**
   * @openapi
   * /api/inspect:
   *   get:
   *     description: inspect locally saved hyperbees
   *     responses:
   *       200:
   *         description: logs stuff
   */
  router.get('/api/inspect', controllers.guides.inspect)

  /**
   * @openapi
   * /:
   *   get:
   *     description: poison pax
   *     responses:
   *       200:
   *         description: pax, ahhhh
   */
  router.get('/', (req, res) => {
    log('Poison Pax!')
    res.sendStatus(200)
  })

  return router
}
