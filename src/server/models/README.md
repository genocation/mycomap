# BeeHive

## Table of Contents

<!-- vim-markdown-toc Marked -->

- [About](#about)
- [Usage](#usage)
- [License](#license)

<!-- vim-markdown-toc -->

## About

a beehive is an arbitrary aggregated set of hypercores, with hyperbee's wrapped around them. beehive uses a `pid` to associate a set of addresses.

swarming and replication is automatically handled by the class, all you need to do is listen for new hyperbees being added and handle them as you want.

remotes are collated in memory as part of the hive using a protocol extension, according to the following flow:

- Elvis receives an invite code from Grace of the format pid@peerBAddress
- Elvis seeds Grace's address into a new beehive class, and creates their own writable feed (and hyperbee) on open
- Elvis sets up protocol extensions on Grace's feed, and when they meet, sends its own feed key to Grace.
- Grace creates a new feed and hyperbee using Elvis's address and begins replicating

if you want to persist your hive, you'll need to store the addresses elsewhere to bootstrap them correctly.

## Usage

in order to create a beehive from a set of addresses, first we need to generate or load a writer, then seed some readers, then open the hive, like this:

```javascript
import RAM from 'random-access-memory'
import Corestore from 'corestore'
import CorestoreNetworker from '@corestore/networker'

const corestore = new Corestore(RAM)
const networker = new Networker(corestore)
const hyperstack = { corestore, networker }

async function main () {
  // the pid is the first 8 characters of the feed address of the hive creator
  const pid = 'f926ef06'
  // pass in the 'client' hyperstack, containing a corestore and @corestore/networker instance
  const hive = new BeeHive(hyperstack, pid)
  // using a key for a writable (we must have the secret_key), corestore will load it and hive will cache it
  const writableKey = null || '6b475434fb90d2050b94dc080241204b14bccc3542419e5b85cdff99f926ef06'
  await hive.writer(writableKey)
  // alternatively, we can pass in a coreOpts object containing a secret key too!
  await hive.writer(writableKey, { secretKey: 'eef20748a84a96a21fa8116b3eab51021c759c309ec3b63420488c370a0f1991' })
  // then any readerable keys we might have, we can pass in as a string or array
  await hive.seed([readableKey])
  // now make all the bees ready in the correct order
  await hive.open()
}

main()
```

we can listen for new remotes being added, you'll want to set this up _before_ calling `hive.open`

```javascript
hive.on('add-bee', (dbee) => {
  // do something with the new bee received from a remote peer
})
```

we can also listen for the hive to be fully open

```javascript
hive.on('open', () => {
  // do something with your loaded hive
  // all feeds and loaded and ready
})
```

## License

`AGPL-3.0-or-later`
