import '../types.js'
import debug from 'debug'
import HyperController from './hyper.js'
import { buildHexColor, buildHashColor } from '../utils.js'

const log = debug('mycomap:api:profile')
const { assign } = Object

export default class ProfileController extends HyperController {
  /**
   * @param {HyperClient} client - an object containing corestore and networker
   * @param {Hyperbee} localdb - hyperbee instance wrapping a primary hypercore
   */
  constructor (client, localdb) {
    super(client, localdb)
    this.get = this.get.bind(this)
    this.put = this.put.bind(this)
  }

  async start () {
    log({
      msg: this.constructor.name,
      payload: {
        profile: await this.#getProfile()
      }
    })
  }

  /**
   * GET /api/profile
   * @param {Object} req - an express request object
   * @param {Object} res - an express response object
   */
  async get (req, res) {
    try {
      const profile = await this.#getProfile()
      profile.nickname = profile.nickname || 'anonymous'
      //profile.color = buildHexColor(profile.nickname)
      profile.color = buildHashColor(profile.nickname)

      return res.json(profile)
    } catch (err) {
      // TODO send message
      return res.sendStatus(404)
    }
  }

  /**
   * POST /api/profile
   * @param {Object} req - an express request object
   * @param {Object} res - an express response object
   */
  async put (req, res) {
    try {
      await this.localdb.put(`profile/${Date.now()}`, req.body)
      return res.sendStatus(200)
    } catch (err) {
      return res.sendStatus(500)
    }
  }

  /**
   * build latest profile from historical profile entries
   * @returns {Profile}
   */
  async #getProfile () {
    const profile = {}
    const stream = this.localdb.createReadStream({ gt: 'profile/', lt: 'profile0' })
    for await (const entry of stream) {
      assign(profile, entry.value, {
        updatedAt: Number(entry.key.split('/')[1])
      })
    }
    return profile
  }
}
