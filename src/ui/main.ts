import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'

import routes from './routes'
import modules from './store'
import App from './App.vue'

Vue.use(Vuex)
Vue.use(VueRouter)

// Leaflet icon fix
// https://vue2-leaflet.netlify.app/quickstart/#marker-icons-are-missing
import { Icon } from 'leaflet'
type D = Icon.Default & { _getIconUrl?: string; }
delete (Icon.Default.prototype as D)._getIconUrl
Icon.Default.mergeOptions({
   iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
   iconUrl: require('leaflet/dist/images/marker-icon.png'),
   shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
})

const router = new VueRouter({routes})
const store = new Vuex.Store(modules)

new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App)
})
