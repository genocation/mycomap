import debug from 'debug'
import { Readable } from 'stream'
import mlts from 'monotonic-lexicographic-timestamp'
import crypto from 'crypto'
const { assign } = Object
const log = debug('mycomap:api:utils')

/*
 * transform a buffer into a hex string
 * @param {string|buffer} buf - a buffer to be hexified
 * @returns {string}
 */
export function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}

/*
 * slice off the first 8 bytes off a 32 byte hex string or buffer
 * @param {string|buffer} buf - the string/buffer to be short hexified
 * @returns {string}
 */
export function shortHex (buf) {
  return hex(buf).substr(0, 8)
}

/*
 * shorten a hash for pretty printing
 * @param {buffer|string} buf
 */
export function prettyHash (buf) {
  if (Buffer.isBuffer(buf)) buf = buf.toString('hex')
  if (typeof buf === 'string' && buf.length > 8) {
    return buf.slice(0, 8) + '...' + buf.slice(-4)
  }
  return buf
}

/*
 * create a monotonic lexicographic comparable id based on Date.now
 * @returns {string}
 */
export const generateID = mlts()

/**
 * sort objects by a given field
 * @param {string|buffer|integer} field - shared object key
 * @returns {function}
 */
export function sortBy (field) {
  return (a, b) => a[field] > b[field]
}

/**
 * create a cloned object with only the key/value pairs specified
 *
 * @param {Object} obj - an object
 * @param {Array} keys - an array of object keys
 */
export function pick (obj, keys) {
  const result = {}
  for (const key of keys) {
    if (obj[key]) result[key] = obj[key]
  }
  return result
}

/**
 * translate a coordinate, latitude or longitude,
 * to a lexicographically comparable string,
 * applying the following transformations:
 * - coord * 1e7
 * - casts to a integer (remove decimals)
 * - normalizes to a positive value (add max*1e7)
 * - casts to a string
 * - left pads to 10 characters
 *
 * the latitude must be between -90 and 90,
 * the longitude must be between -180 and 180
 * we can receive 7 decimals
 *
 * @param {number} coord - a coordinate float
 * @param {number} max - the maximum value of the coordinate
 * @returns {string}
 */
function coordToLex (coord, max) {
  let n = Math.floor(coord * 1e7)
  n += (max * 1e7)
  let s = n.toString()
  let pad = '0000000000' // 10 digits
  return (pad + s).slice(-pad.length)
}

/**
 * translate a lexicographically comparable
 * string to a coordinate, appling the
 * following transformations:
 * - casts string as an integer
 * - normalizes to a real value (subtract max*1e7)
 * - casts as a float
 * - divides by 1e7
 *
 * @param {string} lex - a lexicographically comparable string
 * @param {number} max - the maximum value of the coordinate
 * @returns {number}
 */
function lexToCoord (lex, max) {
  let n = parseInt(lex, 10)
  n -= (max * 1e7)
  return n / 1e7
}

/**
 * converts a latitude float to a lexicographically comparable string
 *
 * @param {number} lat - a latitude float
 * @returns {string} - a lexicographically comparable string
 */
export function latToLex (lat) {
  return coordToLex(lat, 90)
}

/**
 * converts a lexicographically comparable string to a latitude float
 *
 * @param {string} lex - a lexicographically comparable string
 * @returns {number} - a latitude float
 */
export function lexToLat (lex) {
  return lexToCoord(lex, 90)
}

/**
 * converts a lexicographically comparable string to a longitude float
 *
 * @param {number} lex - a lexicographically comparable string
 * @returns {string} - a longitude float
 */
export function lngToLex (lat) {
  return coordToLex(lat, 180)
}

/**
 * converts a lexicographically comparable string to a longitude float
 *
 * @param {string} lex - a lexicographically comparable string
 * @returns {number} - a longitude float
 */
export function lexToLng (lex) {
  return lexToCoord(lex, 180)
}

/**
 * converts an input string into a valid hex color representation
 *
 * @param {string} str - a string to hash and convert to hex color
 * @returns {string} - a string representing a hexadecimal rgb color code
 */
export function buildHexColor (str) {
  let h = str.split('').filter((i,a) => (a%3 == 0)).join('')
  let s = str.split('').filter((i,a) => (a%3 == 1)).join('')
  let l = str.split('').filter((i,a) => (a%3 == 2)).join('')

  h = h.split('').reduce((acc,c)=> c.charCodeAt(0)+acc, 0) % 360
  s = s.split('').reduce((acc,c)=> c.charCodeAt(0)+acc, 0) % 50 + 50
  l = l.split('').reduce((acc,c)=> c.charCodeAt(0)+acc, 0) % 50 + 25

  return `hsl(${h}, ${s}%, ${l}%)`
}

/**
 * converts an input string into a valid hex color representation
 *
 * @param {string} str - a string to hash and convert to hex color
 * @returns {string} - a string representing a hexadecimal rgb color code
 */
export function buildHashColor (str) {
  const hasher = crypto.createHash('md5')
  hasher.update(str)
  const hash = hasher.digest('hex')

  const nums = [
    hash.slice(0, 9),
    hash.slice(10, 19),
    hash.slice(20, 29)
  ].map(hex => parseInt(`0x${hex}`))

  const h = nums[0] % 360
  const s = `${70 + (nums[1] % 30)}%`
  const l = `${30 + (nums[2] % 50)}%`
  return `hsl(${h}, ${s}, ${l})`
}

/**
 * randomly selects one of the four colorizers
 *
 * @param {string} text
 */
export function colourize (code) {
  var code = code || Math.floor(Math.random() * 255)
  return (text) => ['\x1b[38;5;', code, 'm', text, '\x1b[0m'].join('')
}

