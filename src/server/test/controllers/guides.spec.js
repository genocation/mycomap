import fs from 'fs'
import path from 'path'
import { mockRequest, mockResponse, mockReadStream } from '../spec-helper.js'
import GuidesController from '../../controllers/guides.js'

const { assign } = Object
const { list } = JSON.parse(fs.readFileSync(path.join('test', 'fixtures', 'guides.json'), 'utf-8'))

function getPID (key) {
  return key.split('/')[1]
}

function getSID (key) {
  return key.split('/')[2]
}

describe('GuidesController#list', function () {
  it('lists all guide metadata', async function () {
    expect.assertions(1)
    const entries = list.entries.slice(0, 3)
    const guideAPID = getPID(entries[0].ref)
    const guideBPID = getPID(entries[1].ref)
    const guideBSID = getSID(entries[2].ref)
    // setup our mocks
    const req = mockRequest()
    const res = mockResponse()
    // create the controller
    const guidesController = new GuidesController()
    // manually build a mocked guides cache
    const cache = new Map()
    // compile the expected metadata values
    const guideAMeta = {
      id: guideAPID,
      pid: guideAPID,
      name: entries[0].value.name,
      description: entries[0].value.description,
      createdAt: entries[0].value.createdAt,
      createdBy: {},
      updatedAt: entries[0].value.createdAt,
      // updatedBy: {}
      peers: 1
    }
    const guideBMeta = {
      id: guideBPID,
      pid: guideBPID,
      name: entries[1].value.name,
      description: entries[2].value.description,
      createdAt: entries[1].value.createdAt,
      createdBy: {},
      updatedAt: entries[2].value.createdAt,
      // updatedBy: {},
      peers: 2
    }
    // set to the cache
    cache.set(guideAPID, { metadata: jest.fn(() => guideAMeta) })
    cache.set(guideBPID, { metadata: jest.fn(() => guideBMeta) })
    guidesController.guides = cache
    // run the test
    await guidesController.list(req, res)
    expect(res.json).toHaveBeenCalledWith([guideAMeta, guideBMeta])
  })
})
