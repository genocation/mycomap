import '../types.js'
import debug from 'debug'
const log = debug('mycomap:api:hyperspace')

export default class HyperController {
  /**
   * @param {HyperClient} client - an object containing corestore and networker
   * @param {Hyperbee} localdb - hyperbee instance wrapping a primary hypercore
   */
  constructor (client, localdb) {
    this.client = client
    this.localdb = localdb
  }

  async start () {
    // Implement in controllers when asynchronous behavior is required
  }

  async stop () {
    // Implement in controllers when asynchronous behavior is required
  }
}
