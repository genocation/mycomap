import '../types.js'
import assert from 'assert'
import EventEmitter from 'events'
import debug from 'debug'
import Hyperbee from 'hyperbee'
import { Node } from 'hyperbee/lib/messages.js'
import { Readable } from 'stream'
import { inspect, promisify } from 'util'
import * as constants from '../constants.js'
import { hex, shortHex } from '../utils.js'
const { assign } = Object
const log = debug('mycomap:api:models:bee-hive')

export default class BeeHive extends EventEmitter {
  #db = null
  #pid = null
  #cache= new Map()
  #open = null
  #opening = null

  /**
   * @param {HyperClient} client - our hypercore-protocol dependencies (see lib/hyper-client.js)
   * @param {string=} [pid = null] - the primary ID of the bee-hive
   * @param {Object} opts - options
   * @param {string|Object} opts.keyEncoding - key encoding of your hyperbees
   * @param {string|Object} opts.valueEncoding - value encoding of your hyperbees
   */
  constructor (client, pid, opts = {}) {
    super()
    this.client = client
    // pid defaults to null
    this.#pid = pid || null
    // set default hyperbee encoding
    this.encoding = {
      keyEncoding: opts.keyEncoding || constants.defaultEncoding.keyEncoding,
      valueEncoding: opts.valueEncoding || constants.defaultEncoding.valueEncoding
    }
    this.on('remote-bee', this.add.bind(this))
  }

  [Symbol.for('nodejs.util.inspect.custom')] (depth, opts) {
    let indent = ''
    if (typeof opts.indentationLvl === 'number') {
      while (indent.length < opts.indentationLvl) indent += ' '
    }

    return this.constructor.name + '(\n' +
      '  pid: ' + this.pid + '\n' +
      '  sid: ' + this.sid + '\n' +
      '  open: ' + this.#open + '\n' +
      '  db: ' + 'Hyperbee(' + inspect(this.db.feed, { indentationLvl: 2 }) + ')' + '\n' +
      '  bees: ' + this.#inspectBees(indent) + '\n' +
    ')'
  }

  /**
   * db is our writable hyperbee
   * @returns {Hyperbee}
   */
  get db () {
    return this.#db
  }

  /**
   * the unique bee-hive pid
   * @returns {string}
   */
  get pid () {
    return this.#pid
  }

  /**
   * my guide sid is always the short hex of the writable feed key
   * @returns {string}
   */
  get sid () {
    return shortHex(this.db.feed.key)
  }

  /**
   * my guide feed address is always the writable feed key
   * @returns {string}
   */
  get address () {
    return hex(this.db.feed.key)
  }

  /**
   * fetch an invite code for new peers
   * @returns {string}
   */
  get code () {
    return [this.pid, '@', this.address].join('')
  }

  /**
   * cache containing the hive's hyperbees
   * @returns {Map}
   */
  get bees () {
    return this.#cache
  }

  /**
   * list all hyperbee keys - each corresponds to a peer
   * @returns {Array.<string>}
   */
  get peers () {
    const peers = Array.from(this.bees.keys())
    peers.splice(peers.indexOf(hex(this.db.feed.key)), 1)
    return peers
  }

  /**
   * check if the beehive is open
   * @returns {boolean}
   */
  get isOpen () {
    return this.#open
  }

  /**
   * create (in theory) a writable hypercore
   * ATTENTION: if you pass a readable address,
   * this will return a readable feed, so dont!
   * @param {string|null|undefined} address - address of a writable hypercore, or null/undefined for a new one
   * @param {object} opts - additional hypercore options
   * @throws {AssertionError} - beehive is already open
   * @returns {Hyperbee}
   */
  async writer (address, opts) {
    if (this.#db) return this.#db
    assert(!this.#open && !this.#opening, 'already open')
    this.#db = this.#getBee(address, { writable: true, coreOpts: opts })
    await this.db.ready()
    this.#cache.set(hex(this.db.feed.key), this.db)
    return this.db
  }

  /**
   * add, cache and make ready a hyperbee
   * @param {string|null|undefined} address - address of hypercore, or null/undefined for a brand new one
   * @param {object} opts - additional hypercore options
   * @returns {Hyperbee}
   */
  async add (address, opts) {
    const dbee = this.#getBee(address, { coreOpts: opts })
    await dbee.ready()
    this.#cache.set(hex(dbee.feed.key), dbee)
    this.emit('add-bee', dbee)
    return dbee
  }

  /**
   * seed a list of addresses for readable bees
   * into our beehive, this should be called before open
   * @param {Array.<string>} addrs - an array of addresses
   * @throws {AssertionError} - beehive is already open
   */
  seed (addrs) {
    assert(!this.#open && !this.#opening, 'already open')
    if (!Array.isArray(addrs)) addrs = [addrs]
    for (const address of addrs) {
      const dbee = this.#getBee(address)
      this.#cache.set(address, dbee)
    }
  }

  /**
   * open the beehive, makes all bees ready for action!
   * @throws {AssertionError}
   */
  async open () {
    // some basic state management
    assert(!this.#open && !this.#opening, 'already open')
    assert(this.db, 'writer not defined')
    this.#opening = true
    // make sure writer is ready
    await this.db.ready()
    this.#cache.set(hex(this.db.feed.key), this.db)
    // assign the pid if it didnt exist
    if (!this.pid) this.#pid = shortHex(this.db.feed.key)
    // then make all the readers ready
    const proms = []
    for (const [address, dbee] of this.#cache) {
      if (dbee === this.db) continue
      proms.push(dbee.ready())
    }
    await Promise.all(proms)
    // ensure we cannot open twice
    this.#open = true
    this.#opening = false
    // finally emit open to trigger replication on each feed
    this.emit('open')
  }

  /**
   * stop swarming then close all bees and feeds
   */
  async close () {
    const proms = []
    for (const [address, dbee] of this.#cache) {
      await this.client.networker.configure(dbee.feed.discoveryKey, {
        announce: false,
        lookup: false
      })
      proms.push(new Promise((resolve, reject) => {
        dbee.feed.close((err) => err ? reject(err) : resolve())
      }))
    }
    await Promise.all(proms)
    this.#db = null
    this.#pid = null
  }

  /**
   * create a read stream of all hyperbees merged together
   * defaults to object mode
   * @param {object} opts - a readable stream options
   * @returns {Readable}
   */
  createReadStream (opts = { objectMode: true }) {
    const self = this
    return Readable.from(iterator(), opts)

    async function* iterator (query) {
      for (const dbee of self.#cache.values()) {
        const stream = dbee.createReadStream(opts)
        for await (const entry of stream) {
          yield entry
        }
      }
    }
  }

  /**
   * replicate all bee feeds
   * @param {object} opts
   */
  replicate (opts = {}) {
    for (const dbee of this.#cache.values()) {
      this.replicateFeed(dbee.feed, opts)
    }
  }

  /**
   * replicate a single bee feed
   * @param {Hyperbee} dbee - a hyperbee instance
   * @param {object} opts
   */
  replicateFeed (feed, opts = {}) {
    this.client.networker.configure(feed.discoveryKey, {
      ...opts,
      announce: true,
      lookup: true
    })
  }

  // ------------------------------ PRIVATE -------------------------------- //

  /**
   * get a feed and hyperbee from corestore
   * @param {string|null|undefined} address - a 32 byte hex string (64 characters)
   * @param {object} opts - setup extension options
   * @param {object} opts.coreOpts - hypercore opts
   * @returns {Hyperbee}
   */
  #getBee (address, opts = {}) {
    if (this.#cache.has(address)) return this.#cache.get(address)
    const coreOpts = assign({ key: address }, opts.coreOpts || {})
    const feed = this.client.corestore.get(coreOpts)
    const dbee = new Hyperbee(feed, this.encoding)
    this.#setupExtension(feed, opts)
    feed.on('download', (seq, data) => {
      try {
        const decoded = Node.decode(data)
        const address = hex(feed.key)
        const entry = {
          seq,
          address,
          key: decoded.key.toString(this.encoding.keyEncoding),
          value: JSON.parse(decoded.value.toString(this.encoding.keyEncoding))
        }
        this.emit('new-entry', entry)
      } catch (err) {
        log({ msg: '[ON-DOWNLOAD] failed to decode new entry', err })
      }
    })
    feed.on('ready', () => this.replicateFeed(feed))
    return dbee
  }

  /**
   * setup a protocol extension to enable new peers to become writers
   * - if feed is readonly, send the address of the writable feed so
   *   remote peers address will include the feed address
   * - if feed is writable, then we simply listen for the address
   * @param {Hypercore} feed - a hypercore feed
   * @param {object} opts
   * @param {boolean} opts.writable
   */
  #setupExtension (feed, opts = { writable: false }) {
    const self = this
    // TODO: send a manifest with a protocol name and version number
    // as a protobuffer, then decode and validate essage
    // and change the encoding type to binary, this will be quicker
    // {
    //   name: 'BEE-HIVE',
    //   version: '0.0.1',
    //   payload: {
    //     type: 'forward',
    //     pid: '45cc4bb',
    //     address: 'ef76d3e719546ded49167caf08757c532312563f12ac7e2d4d168553f45cc4bb'
    //   }
    // }
    const extension = feed.registerExtension('bee-hive', assign({ encoding: 'json' }, {
      /**
       * for all feeds, listen for new addresses, emit ones we don't have
       * this will add a new hyperbee to the cache, setup extensions and start replicating
       * @throws {AssertionError}
       */
      onmessage (message, peer) {
        assert(typeof message === 'object' && message.pid && message.address, 'invalid extension message')
        const { pid, address } = message
        if (self.bees.has(address)) return
        self.emit('remote-bee', address)
      },
      onerror (err) {
        log({ msg: '[EXTENSION ERROR]', err })
      }
    }))

    if (!opts.writable) {
      /**
       * for readable feeds, send our address to remote peer
       * so they include us and add our feed to their bee-hive
       */
      feed.on('peer-open', function sendAddress (peer) {
        const peerId = shortHex(peer.remotePublicKey)
        const message = { pid: self.pid, address: self.address }
        extension.send(message, peer)
      })
    }
  }

  /**
   * pretty print the bees cache
   * @param {string} indent - the indentation level
   */
  #inspectBees (indent) {
    let bees = indent + '{'
    for (const [address, dbee] of this.bees) {
      bees +=  '\n' + indent + '    ' +
        shortHex(address) + ': ' +
        // ATTENTION:
        // currently inspects the hypercore (which is prettier
        // and more useful) but the actual object is a hyperbee
        'Hyperbee(' + inspect(dbee.feed, { indentationLvl: 4 }) + ')'
    }
    if (this.bees.size) bees += '\n'
    bees += (indent + '  }')
    return bees
  }
}
